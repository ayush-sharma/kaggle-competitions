__author__ = 'ayush'

import csv
import numpy
import time
import datetime
from pymongo import MongoClient
import math

import matplotlib.pyplot as plt
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.datasets.samples_generator import make_blobs
import cPickle

# try coords - accuracy
# try svm
# try clustering
# try clustering based on x,y coords; then based on coords choose regions(2 or 3); based on time - rank place_ids

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
db = client['meteor']['fbtrain']

def plot_places(k_means_labels_unique, k_means_labels, k_means_cluster_centers, n_clusters, X_coords):
	colors = plt.cm.Spectral(numpy.linspace(0, 1, len(k_means_labels_unique)))
	# colors = ['#4EACC5', '#FF9C34', '#4E9A06']

	for k, col in zip(range(n_clusters), colors):
	    my_members = k_means_labels == k
	    # print "my_members", my_members, my_members.size, my_members.shape
	    # exit(0)
	    cluster_center = k_means_cluster_centers[k]
	    plt.plot(X_coords[my_members, 0], X_coords[my_members, 1], 'w', markerfacecolor=col, marker='.')
	    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=6)
	    plt.text(cluster_center[0], cluster_center[1], str(k))
	plt.show()

def getDataByKMId(KMId, k_means_labels, X_id):
    return [X_id[i] for i, x in enumerate(k_means_labels) if x == KMId]

def are_overlapping(box1, box2):
	if box1["lat_min"] > box2["lat_max"] or box2["lat_min"] > box1["lat_max"]:
		return False
	if box1["lon_max"] < box2["lon_min"] or box2["lon_max"] < box1["lon_min"]:
		return False
	return True

def getClusterData(load_data, n_clusters = 100):
	print "in getClusterData"

	if not load_data:
		# data = db.find({}, {"_id": 0, "x": 1, "y": 1, "row_id": 1})
		# print "fetching data ..."
		# X_coords = []
		# X_id = []
		# count = 0
		# saving_index = 0
		# for doc in data:
		# 	count += 1
		# 	if count % 10000 == 0:
		# 		print count

		# 	if count % 5000000 == 0:
		# 		numpy.save('X_coords' + str(saving_index) + '.npy', X_coords)
		# 		numpy.save('X_row_id' + str(saving_index) + '.npy', X_id)
		# 		print "saved", count, saving_index
		# 		X_coords = []
		# 		X_id = []
		# 		saving_index += 1

		# 	X_coords.append([doc["x"], doc["y"]])
		# 	X_id.append(doc["row_id"])
		
		# if X_coords:
		# 	numpy.save('X_coords' + str(saving_index) + '.npy', X_coords)
		# 	numpy.save('X_row_id' + str(saving_index) + '.npy', X_id)
		# 	print "saved", count, saving_index
		# 	X_coords = []
		# 	X_id = []
		# print "X created"

		X_coords = numpy.load('X_coords.npy')
		# X_row_id = numpy.load('X_row_id.npy')

		print "clustering ..."

		# k_means = KMeans(init='k-means++', verbose=1, n_clusters=n_clusters, n_init=10)
		k_means = MiniBatchKMeans(init='k-means++', verbose=1, batch_size=1000, n_clusters=n_clusters, n_init=10)

		k_means.fit(X_coords)
		with open('kmeans_model.pkl', 'wb') as fid:
		    cPickle.dump(k_means, fid)
		print "saved"
		exit(0)
	else:
		X_coords = numpy.load('X_coords.npy')
		X_id = numpy.load('X_row_id.npy')	

		k_means = None
		with open('kmeans_model.pkl', 'rb') as fid:
		    k_means = cPickle.load(fid)
		print "loaded"

	return X_coords, X_id, k_means


if __name__ == "__main__":

	load_data = False
	n_clusters = 10000

	X_coords, X_id, k_means = getClusterData(load_data, n_clusters)

	print "returned from getClusterData"

	k_means_labels = k_means.labels_
	k_means_cluster_centers = k_means.cluster_centers_
	k_means_labels_unique = numpy.unique(k_means_labels)

	print "predicting sample ..."

	match_id = k_means.predict(numpy.array([[3.0,3.0]]))[0]
	print match_id
	eee = getDataByKMId(match_id, k_means_labels, X_id)
	print eee[0:5]
	print len(eee)

	print "plotting ..."

	plot_places(k_means_labels_unique, k_means_labels, k_means_cluster_centers, n_clusters, X_coords)

	print "done"