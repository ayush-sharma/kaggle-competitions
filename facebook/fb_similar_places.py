__author__ = 'ayush'

from pymongo import MongoClient
import math

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
db = client['meteor']['fbtrain']

def sim_distance_coords(place1_id,place2_id):

	place1 = db.find_one({"row_id": place1_id})
	place2 = db.find_one({"row_id": place2_id})

	# print "place1", place1["time"]
	# print "place2", place2["time"]
	
	sum_of_squares = 0

	sum_of_squares += pow(place1["x"] - place2["x"], 2)
	sum_of_squares += pow(place1["y"] - place2["y"], 2)

	return float(1)/(1+sum_of_squares), place2

def sim_distance(place1_id,place2_id):

	place1 = db.find_one({"row_id": place1_id})
	place2 = db.find_one({"row_id": place2_id})

	# print "place1", place1["time"]
	# print "place2", place2["time"]
	
	sum_of_squares = 0

	for entity in place1["time"]:
		sum_of_squares += pow(place1["time"][entity] - place2["time"][entity], 2)

	return float(1)/(1+sum_of_squares), place2

def sim_pearson(place1_id,place2_id):

	place1 = db.find_one({"row_id": place1_id})
	place2 = db.find_one({"row_id": place2_id})

	# print "place1", place1["time"]
	# print "place2", place2["time"]

	n = len(place1["time"])

	list1 = [place1["time"][entity] for entity in place1["time"]]
	list2 = [place2["time"][entity] for entity in place2["time"]]

	sum1 = sum(list1)
	sum2 = sum(list2)

	sum1Sq = sum([pow(i, 2) for i in list1])
	sum2Sq = sum([pow(i, 2) for i in list2])

	pSum = sum([entity1 * list2[i] for i, entity1 in enumerate(list1)])

	num = pSum - (sum1 * sum2 / float(n))
	den = math.sqrt((sum1Sq - pow(sum1, 2) / float(n)) * (sum2Sq - pow(sum2, 2) / float(n)))
	if den == 0: 
		return 0
	r = num / float(den)
	return r, place2

def top_matches(place1_id, nearby_places_ids, top=100, similarity=sim_distance):
	scores = []
	for place2_id in nearby_places_ids:
		score, rec_place = similarity(place1_id, place2_id)
		scores.append((score, rec_place))
	scores.sort(reverse=True)
	return scores[0:top]

def top_matches_coords(place1_id, same_cluster_place_ids, top=100, similarity=sim_distance_coords):
	scores = []
	for place2_id in same_cluster_place_ids:
		score, rec_place = similarity(place1_id, place2_id)
		scores.append((score, rec_place))
	scores.sort(reverse=True)
	return scores[0:top]


if __name__ == "__main__":
	print sim_distance(102598, 102597)
	print sim_distance(102598, 102596)

	print sim_pearson(102598, 102597)
	print sim_pearson(102598, 102596)