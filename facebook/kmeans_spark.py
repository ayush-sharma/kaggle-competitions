__author__ = 'ayush'

from pyspark.mllib.clustering import KMeans, KMeansModel
import numpy
from pymongo import MongoClient
from pyspark import SparkContext
sc = SparkContext()

X_coords = numpy.load('X_coords.npy')
X_row_id = numpy.load('X_row_id.npy')

print "clustering ..."

x_train = sc.parallelize(X_coords)

# x_train = x_train.take(5)
# x_train = sc.parallelize(x_train)

clusters = KMeans.train(x_train, 10000, maxIterations=10, runs=10, initializationMode="random")

clusters.save(sc, 'spark_model')

print "saved"

# x_test = sc.parallelize(numpy.array([x_train[]]))

exit(0)

data = numpy.array([[1,2,3], [4,5,6], [7,8,9]])

x_train = sc.parallelize(data)

# clusters = KMeans.train(x_train, 3, maxIterations=10, runs=10, initializationMode="random")
clusters = KMeansModel.load(sc, 'spark_model')

print dir(clusters)

print "here", clusters.centers

x_test = sc.parallelize(numpy.array([[1,2,3], [4,5,6]]))

print "here1", clusters.predict(x_test).collect()

print "here2", clusters.k

# clusters.save(sc, 'spark_model')