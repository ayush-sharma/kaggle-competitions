__author__ = 'ayush'

import csv
# import numpy
import time
import datetime
from pymongo import MongoClient
import math

# try coords - accuracy
# try svm
# try clustering
# try clustering based on x,y coords; then based on coords choose regions(2 or 3); based on time - rank place_ids

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
db = client['meteor']['fbtrain']


def get_bounding_box(latitude_in_degrees, longitude_in_degrees, half_side_in_meters):
    assert half_side_in_meters > 0
    assert latitude_in_degrees >= -90.0 and latitude_in_degrees  <= 90.0
    assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

    half_side_in_km = half_side_in_meters / 100
    lat = math.radians(latitude_in_degrees)
    lon = math.radians(longitude_in_degrees)

    radius  = 6371
    # Radius of the parallel at given latitude
    parallel_radius = radius*math.cos(lat)

    lat_min = lat - half_side_in_km/radius
    lat_max = lat + half_side_in_km/radius
    lon_min = lon - half_side_in_km/parallel_radius
    lon_max = lon + half_side_in_km/parallel_radius
    rad2deg = math.degrees

    lat_min = rad2deg(lat_min)
    lon_min = rad2deg(lon_min)
    lat_max = rad2deg(lat_max)
    lon_max = rad2deg(lon_max)

    return {"lat_min": lat_min, "lon_min": lon_min, "lat_max": lat_max, "lon_max": lon_max}

data = db.find()

count = 0
for doc in data:
	count += 1
	print count
	box = get_bounding_box(doc["x"], doc["y"], doc["accuracy"])
	db.update({"_id": doc["_id"]}, {"$set": {"box": box}})
