from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import numpy

data = ["Joe was great in the meeting today",
		"It looks like the project will meet the deadlines",
		"However, we may be overshooting the budget",
		"Please talk to him",
		"Also, he needs to improve on his conflict resolution skills",
		"Takes comments too personally",
		"Mike is pretty rude in the meetings",
		"However, his project management is very good",
		"We are always on time and on budget, on his projects",
		"But that alone doesn't make him a good employee",
		"He needs to change; if not, we will have to look for someone else",
		"Linda is a great worker",
		"She needs to focus on her presentation skills though",
		"Also, the project seems headed for cost overruns",
		"Helen is an average worker",
		"She finds it difficult to follow high level guidance",
		"Needs lots of handholding",
		"Mike is pretty presentable in the meetings",
		"Brad is an awesome coder",
		"He needs to focus on his presentation skills and temper"]

# label = [1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0]

class_name = ["general",
			"deadline",
			"conflict",
			"conflict",
			"conflict",
			"conflict",
			"general",
			"deadline",
			"deadline",
			"conflict",
			"conflict",
			"general",
			"general",
			"deadline",
			"general",
			"conflict",
			"conflict",
			"general",
			"conflict",
			"conflict"]

unique_class_names = numpy.unique(class_name)
unique_class_names = unique_class_names.tolist()

label = [unique_class_names.index(i) for i in class_name]
print "unique_class_names", unique_class_names
print "label", label

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(data)


tf_transformer = TfidfTransformer(use_idf=False).fit(X_train_counts)
X_train_tf = tf_transformer.transform(X_train_counts)


tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)


clf = MultinomialNB().fit(X_train_tfidf, label)


# docs_new = [data[4], data[7]]
# actual_labels = [label[4], label[7]]

docs_new = data
actual_labels = label

X_new_counts = count_vect.transform(docs_new)
X_new_tfidf = tfidf_transformer.transform(X_new_counts)

predicted = clf.predict(X_new_tfidf)
print "predicted", predicted
print "actual_labels", actual_labels

print "accuracy", numpy.mean(predicted == actual_labels)