__author__ = 'ayush'

from pyspark import SparkContext
from pyspark.streaming import StreamingContext

# Create a local StreamingContext with two working thread and batch interval of 1 second
sc = SparkContext("local[2]", "NetworkWordCount")
ssc = StreamingContext(sc, 5)

wordCounts = {}

lines = ssc.socketTextStream("localhost", 9999)

def parse(line):
	print "line", line
	elems = line.split(' ')
	for i in elems:
		wordCounts.setdefault(i, 1)
		wordCounts[i] += 1

# words = lines.flatMap(parse)

words = lines.flatMap(lambda line: line.split(" "))
pairs = words.map(lambda word: (word, 1))
wordCounts = pairs.reduceByKey(lambda x, y: x + y)
wordCounts.pprint()
# print "words", words.pprint()
# print "wordCounts", wordCounts

ssc.start()             # Start the computation
ssc.awaitTermination()  # Wait for the computation to terminate