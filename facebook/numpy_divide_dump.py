__author__ = 'ayush'

import csv
import numpy
import time
import datetime
from bisect import bisect_left


def take_closest(arange, num):
    pos = bisect_left(arange, num)
    if pos == 0:
        return arange[0]
    if pos == len(arange):
        return arange[-1]
    before = arange[pos - 1]
    after = arange[pos]
    if after - num < num - before:
       return after
    else:
       return before

def get_closest_cluster(x_arange, y_arange, coord):
	x = take_closest(x_arange, coord[0])
	y = take_closest(y_arange, coord[1])
	return x, y


if __name__ == "__main__":
	data = {}

	x_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
	y_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)

	with open('train.csv', 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		headers = next(csvreader, None)
		time_keys = ['year', 'month', 'day', 'hour', 'min', 'sec', 'wday', 'yday', 'isdst']
		for count, line in enumerate(csvreader):
			if count % 10000 == 0:
				print "count", count
			row = {}
			for index, cell in enumerate(line):
				parsed = float(cell)
				if headers[index] != "time":
					row[headers[index]] = parsed
				else:
					time_str = datetime.datetime.fromtimestamp(int(parsed)).strftime('%Y-%m-%d %H:%M:%S')
					time_dict = {}
					cell_strptime = time.strptime(time_str, "%Y-%m-%d %H:%M:%S")
					for ind, ent in enumerate(cell_strptime):
						time_dict[time_keys[ind]] = ent
					row[headers[index]] = time_dict

			x, y = get_closest_cluster(x_limit_arange, y_limit_arange, [row["x"], row["y"]])
			x = round(x, 2)
			y = round(y, 2)
			data.setdefault(x, {})
			data[x].setdefault(y, [])
			data[x][y].append(row)

	numpy.save('data_divide.npy', data)
	print "done"


