__author__ = 'ayush'

import numpy
import time
from pymongo import MongoClient
import kmeans_fb
import fb_similar_places

# try coords - accuracy
# try svm
# try clustering
# try clustering based on x,y coords; then based on coords choose regions(2 or 3); based on time - rank place_ids

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
db = client['meteor']['fbtrain']

def are_overlapping(box1, box2):
	if box1["lat_min"] > box2["lat_max"] or box2["lat_min"] > box1["lat_max"]:
		return False
	if box1["lon_max"] < box2["lon_min"] or box2["lon_max"] < box1["lon_min"]:
		return False
	return True

# start_time = time.time()
# overlap = []
# for doc in data:
# 	if are_overlapping(one_doc["box"], doc["box"]):
# 		overlap.append(doc["_id"])

# print overlap
# print "time taken", time.time() - start_time

def getTopOverlappingPlaces(place1_id, same_cluster_place_ids, top=100):
	place1 = db.find_one({"row_id": place1_id}, {"time":0, "accuracy":0, "x":0, "y":0, "_id":0})
	same_cluster_places = db.find({"row_id": {"$in": same_cluster_place_ids}}, {"time":0, "accuracy":0, "x":0, "y":0, "_id":0})

	nearby_places_ids = []

	for place2 in same_cluster_places:
		if are_overlapping(place1["box"], place2["box"]):
			nearby_places_ids.append(place2["row_id"])
		if len(nearby_places_ids) >= top:
			return nearby_places_ids

	return nearby_places_ids

if __name__ == "__main__":

	data = db.find({"row_id": 44757.0})

	X_coords, X_id, k_means = kmeans_fb.getClusterData(True)

	k_means_labels = k_means.labels_
	k_means_cluster_centers = k_means.cluster_centers_
	k_means_labels_unique = numpy.unique(k_means_labels)

	for doc in data:
		matched_cluster_id = k_means.predict(numpy.array([[float(doc["x"]), float(doc["y"])]]))[0]
		same_cluster_place_ids = kmeans_fb.getDataByKMId(matched_cluster_id, k_means_labels, X_id)
		print "same_cluster_place_ids", len(same_cluster_place_ids)
		# nearby_places_ids = getTopOverlappingPlaces(doc["row_id"], same_cluster_place_ids, top=len(same_cluster_place_ids))
		nearby_places = fb_similar_places.top_matches_coords(doc["row_id"], same_cluster_place_ids, top=len(same_cluster_place_ids))
		nearby_places_ids = [i[1]["row_id"] for i in nearby_places]
		# nearby_places_ids = same_cluster_place_ids
		print "nearby_places_ids", nearby_places_ids, "\n\n\n"
		# exit(0)
		recommendations = fb_similar_places.top_matches(doc["row_id"], nearby_places_ids, top=len(nearby_places_ids))
		print doc
		# print "recommendations", recommendations
		for rec in recommendations:
			if rec[1]["place_id"] == doc["place_id"]:
				result = True
			else:
				result = False
			print rec[0], result, "\t", rec[1]
		exit(0)
