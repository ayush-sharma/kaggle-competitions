__author__ = 'ayush'

# from pyspark.mllib.clustering import KMeans, KMeansModel
from pyspark.mllib.clustering import StreamingKMeans
import numpy
from pprint import pprint
import time
# from pymongo import MongoClient
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
sc = SparkContext()
ssc = StreamingContext(sc, 20)
sc.setLogLevel("ERROR")

rddQueue = []
for i in range(5):
	rddQueue += [ssc.sparkContext.parallelize([j for j in range(1, 1001)], 10)]

# Create the QueueInputDStream and use it do some processing
inputStream = ssc.queueStream(rddQueue)
mappedStream = inputStream.map(lambda x: (x % 10, 1))
reducedStream = mappedStream.reduceByKey(lambda a, b: a + b)
reducedStream.pprint()

ssc.start()
time.sleep(6)
ssc.stop(stopSparkContext=True, stopGraceFully=True)






# sc = SparkContext()
# ssc = StreamingContext(sc, 20)

# lines = ssc.textFileStream("/media/Windows/ayush_backup/kaggle/facebook/streaming_data/train")
# counts = lines.flatMap(lambda line: line.split(","))\
#               .map(lambda x: (x, 1))\
#               .reduceByKey(lambda a, b: a+b)
# print "here"
# counts.pprint()

# ssc.start()
# ssc.awaitTermination()


