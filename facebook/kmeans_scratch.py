__author__ = 'ayush'

import numpy
from pymongo import MongoClient
import math
import time

# X_coords = numpy.load('X_coords.npy')
# X_row_id = numpy.load('X_row_id.npy')

def find_closest_centroids(X, centroids):
	k = centroids.shape[0]
	idx = numpy.array([])
	m, n = X.shape

	for i in range(0, m):
		min_distance = None
		min_k = None
		for j in range(0, k):
			distance = sum(pow((X[i, :] - centroids[j, :]), 2))
			if min_distance == None or min_distance < distance:
				min_distance = distance
				min_k = j
		idx = numpy.append(idx, min_k)

	return idx

def compute_centroids(X, idx, k):
	m, n = X.shape
	centroids = numpy.zeros([k, X.shape[1]])

	for i in range(0, n):
		for j in range(0, k):
			index = idx == j
			arr = X[index, i]
			centroids[j, i] = sum(arr)/float(len(arr))

	return centroids

def run_kmeans(X, init_centroids, max_iters):
	m, n = X.shape
	k = init_centroids.shape[0]
	centroids = init_centroids
	idx = None

	for iters in range(0, max_iters):
		print "iter", iters
		idx = find_closest_centroids(X, centroids)
		print "got idx"
		centroids = compute_centroids(X, idx, k)
		print "got centroids"

	return centroids, idx


if __name__ == "__main__":

	start_time = time.time()

	X_coords = numpy.load('X_coords.npy')
	# X_row_id = numpy.load('X_row_id.npy')

	n_cluster = 10000
	max_iters = 1

	factor = 10/math.sqrt(n_cluster)
	factor_arange = numpy.arange(0, 10, factor)
	factor_arange = [i+(float(factor)/2) for i in factor_arange]

	init_centroids = []

	for i in factor_arange:
		for j in factor_arange:
			init_centroids.append([i, j])
	init_centroids = numpy.array(init_centroids)

	print "clustering ..."

	centroids, idx = run_kmeans(X_coords, init_centroids, max_iters)

	numpy.save('centroids.npy', centroids)
	numpy.save('idx.npy', idx)

	print "saved", time.time() - start_time

