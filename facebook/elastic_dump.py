__author__ = 'ayush'

import csv
# import numpy
import time
import datetime
# from pymongo import MongoClient

from elasticsearch import Elasticsearch
from elasticsearch import helpers

# client = MongoClient('mongodb://127.0.0.1:3001/meteor')
# db = client['meteor']['fbtrain']
# db.remove({})

es = Elasticsearch()

bulk_docs = []
bulk_count = 10000

with open('train.csv', 'r') as csvfile:
	csvreader = csv.reader(csvfile)
	headers = next(csvreader, None)
	time_keys = ['year', 'month', 'day', 'hour', 'min', 'sec', 'wday', 'yday', 'isdst']
	for count, line in enumerate(csvreader):
		row = {}
		for index, cell in enumerate(line):
			parsed = float(cell)
			if headers[index] != "time":
				row[headers[index]] = parsed
			else:
				time_str = datetime.datetime.fromtimestamp(int(parsed)).strftime('%Y-%m-%d %H:%M:%S')
				time_dict = {}
				cell_strptime = time.strptime(time_str, "%Y-%m-%d %H:%M:%S")
				for ind, ent in enumerate(cell_strptime):
					time_dict[time_keys[ind]] = ent
				# print "time_str", time_str
				row[headers[index]] = time_dict
		bulk_docs.append(row)

		if count % bulk_count == 0:
			# db.insert_many(bulk_docs)
			helpers.bulk(es, bulk_docs, index="coords", doc_type="fbtrain")
			print "inserted", count
			bulk_docs = []

if bulk_docs:
	# db.insert_many(bulk_docs)
	helpers.bulk(es, bulk_docs, index="coords", doc_type="fbtrain")
	print "inserted last bulk"

print "done"


