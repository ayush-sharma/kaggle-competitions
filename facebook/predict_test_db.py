__author__ = 'ayush'

import csv
import datetime
from pymongo import MongoClient
import numpy
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
# from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import log_loss
from bisect import bisect_left
import time
import subprocess


x_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
y_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
top_features = [228, 229, 237, 245, 121, 138, 83, 112, 119, 120, 153, 207, 217, 221, 222, 227, 267, 270, 21, 43, 84, 102, 103, 130, 208, 218, 268, 42, 55, 137, 145, 146, 182, 210, 258]
time_keys = ['year', 'month', 'day', 'hour', 'min', 'sec', 'wday', 'yday', 'isdst']


def take_closest(arange, num):
    pos = bisect_left(arange, num)
    if pos == 0:
        return arange[0]
    if pos == len(arange):
        return arange[-1]
    before = arange[pos - 1]
    after = arange[pos]
    if after - num < num - before:
       return after
    else:
       return before

def get_closest_cluster(x_arange, y_arange, coord):
	x = take_closest(x_arange, coord[0])
	y = take_closest(y_arange, coord[1])
	return round(x, 1), round(y, 1)


def features_product(x):
	x_feat = None
	for i in xrange(0, x.shape[1] - 1):
		for j in xrange(i+1, x.shape[1]):
			col = (x[:, i]*x[:, j]).reshape(x.shape[0], 1)
			if x_feat == None:
				x_feat = col
			else:
				x_feat = numpy.hstack((x_feat, col))
	return x_feat

def get_features(x):
	x_feat_sq = pow(x, 2)
	x_feat_cube = pow(x, 3)
	x_feat = numpy.hstack((x, x_feat_sq, x_feat_cube))
	x_feat = features_product(x_feat)
	return x_feat


def create_x(doc):
	return [doc["x"], doc["y"], doc["time"]["year"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"], doc["time"]["min"]]
	# return [doc["x"], doc["y"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]


def check_top_n_prob(probabilities, LR_labels, top=3):
	probability = probabilities[0]
	sorted_prob = sorted(range(len(probability)), reverse=True, key=lambda k: probability[k])[:top]
	sorted_labels = [int(LR_labels[i]) for i in sorted_prob]
	return sorted_labels




def create_doc(headers, line):
	row = {}
	for index, cell in enumerate(line):
		parsed = float(cell)
		if headers[index] != "time":
			row[headers[index]] = parsed
		else:
			time_str = datetime.datetime.fromtimestamp(int(parsed)).strftime('%Y-%m-%d %H:%M:%S')
			time_dict = {}
			cell_strptime = time.strptime(time_str, "%Y-%m-%d %H:%M:%S")
			for ind, ent in enumerate(cell_strptime):
				time_dict[time_keys[ind]] = ent
			row[headers[index]] = time_dict
	return row

def get_trained_model(test_x, test_y):

	doc_x, doc_y = get_closest_cluster(x_limit_arange, y_limit_arange, [test_x, test_y])

	model = None

	try:
		model = joblib.load('LR_models/LR_model_' + str(doc_x) + '_' + str(doc_y) + '.pkl')
		# print "model found!!YAY!!!"
	except Exception as e:
		print "model not found"
		cluster = clusters_db.find_one({"x": doc_x, "y": doc_y})
	
		row_ids = cluster["row_ids"]

		row_id = row_ids.pop(0)
		doc = fbtrain_db.find_one({"row_id": row_id})
		x = create_x(doc)
		
		x_train = numpy.array([x])
		y_train = numpy.array([doc["place_id"]])

		docs = fbtrain_db.find({"row_id": {"$in": row_ids}})

		for doc in docs:
			x = create_x(doc)

			x_train = numpy.vstack((x_train, numpy.array(x)))
			y_train = numpy.append(y_train, doc["place_id"])

		x_all = get_features(x_train)

		x_train = x_all[:, top_features]

		model = LogisticRegression()
		print "training...", len(row_ids)
		model.fit(x_train, y_train)
		joblib.dump(model, 'LR_models/LR_model_' + str(doc_x) + "_" + str(doc_y) + '.pkl', compress=1)
		print "saved"

	return model


if __name__ == "__main__":
	
	client = MongoClient('mongodb://127.0.0.1:3001/meteor')
	# fbtrain_db = client['meteor']['fbtrain']
	# clusters_db = client['meteor']['clusters']
	fbtest_db = client['meteor']['fbtest']

	prediction_1_2_3_file = open('prediction_1_2_3.csv','w')
	prediction_1_3_2_file = open('prediction_1_3_2.csv','w')
	prediction_2_1_3_file = open('prediction_2_1_3.csv','w')
	prediction_2_3_1_file = open('prediction_2_3_1.csv','w')
	prediction_3_1_2_file = open('prediction_3_1_2.csv','w')
	prediction_3_2_1_file = open('prediction_3_2_1.csv','w')

	prediction_1_file = open('prediction_1.csv','w')
	prediction_2_file = open('prediction_2.csv','w')
	prediction_3_file = open('prediction_3.csv','w')

	prediction_1_2_file = open('prediction_1_2.csv','w')
	prediction_2_1_file = open('prediction_2_1.csv','w')
	prediction_1_3_file = open('prediction_1_3.csv','w')
	prediction_3_1_file = open('prediction_3_1.csv','w')
	prediction_2_3_file = open('prediction_2_3.csv','w')
	prediction_3_2_file = open('prediction_3_2.csv','w')

	# prediction_1_2_3_file.write('row_id, place_id\n')
	# prediction_1_3_2_file.write('row_id, place_id\n')
	# prediction_2_1_3_file.write('row_id, place_id\n')
	# prediction_2_3_1_file.write('row_id, place_id\n')
	# prediction_3_1_2_file.write('row_id, place_id\n')
	# prediction_3_2_1_file.write('row_id, place_id\n')
	# prediction_1_file.write('row_id, place_id\n')
	# prediction_2_file.write('row_id, place_id\n')
	# prediction_3_file.write('row_id, place_id\n')
	# prediction_1_2_file.write('row_id, place_id\n')
	# prediction_2_1_file.write('row_id, place_id\n')
	# prediction_1_3_file.write('row_id, place_id\n')
	# prediction_3_1_file.write('row_id, place_id\n')
	# prediction_2_3_file.write('row_id, place_id\n')
	# prediction_3_2_file.write('row_id, place_id\n')

	test_data = fbtest_db.find({"_id": {"$gt": 122194}}).sort("_id", 1)

	for doc in test_data:
		count = doc["_id"]
		if count % 10000 == 0:
			print "count", count

		model = get_trained_model(doc["x"], doc["y"])

		doc = create_x(doc)
		doc = numpy.array([doc])
		x_test_all = get_features(doc)
		x_test = x_test_all[:, top_features]
		# print model.predict(x_test)

		probabilities = model.predict_proba(x_test)
		preds = check_top_n_prob(probabilities, model.classes_)


		prediction_1_2_3_file.write(str(count) + ", " + str(preds[0]) + " " + str(preds[1]) + " " + str(preds[2]) + "\n")
		prediction_1_3_2_file.write(str(count) + ", " + str(preds[0]) + " " + str(preds[2]) + " " + str(preds[1]) + "\n")
		prediction_2_1_3_file.write(str(count) + ", " + str(preds[1]) + " " + str(preds[0]) + " " + str(preds[2]) + "\n")
		prediction_2_3_1_file.write(str(count) + ", " + str(preds[1]) + " " + str(preds[2]) + " " + str(preds[0]) + "\n")
		prediction_3_1_2_file.write(str(count) + ", " + str(preds[2]) + " " + str(preds[0]) + " " + str(preds[1]) + "\n")
		prediction_3_2_1_file.write(str(count) + ", " + str(preds[2]) + " " + str(preds[1]) + " " + str(preds[0]) + "\n")

		prediction_1_file.write(str(count) + ", " + str(preds[0]) + "\n")
		prediction_2_file.write(str(count) + ", " + str(preds[1]) + "\n")
		prediction_3_file.write(str(count) + ", " + str(preds[2]) + "\n")

		prediction_1_2_file.write(str(count) + ", " + str(preds[0]) + " " + str(preds[1]) + "\n")
		prediction_2_1_file.write(str(count) + ", " + str(preds[1]) + " " + str(preds[0]) + "\n")
		prediction_1_3_file.write(str(count) + ", " + str(preds[0]) + " " + str(preds[2]) + "\n")
		prediction_3_1_file.write(str(count) + ", " + str(preds[2]) + " " + str(preds[0]) + "\n")
		prediction_2_3_file.write(str(count) + ", " + str(preds[1]) + " " + str(preds[2]) + "\n")
		prediction_3_2_file.write(str(count) + ", " + str(preds[2]) + " " + str(preds[1]) + "\n")


			# preds_str = ""
			# for pred in preds:
			# 	preds_str += str(pred) + " "
			# prediction_file.write(str(count) + ',' + preds_str + "\n")
			
