__author__ = 'ayush'

# from pymongo import MongoClient
import kmeans_fb
import numpy
from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import SVC
from sklearn import datasets
from sklearn.externals import joblib
from sklearn.feature_selection import VarianceThreshold
from bisect import bisect_left
import math
from elasticsearch import Elasticsearch
from elasticsearch import helpers

es = Elasticsearch()

# client = MongoClient('mongodb://127.0.0.1:3001/meteor')
# db = client['meteor']['fbtrain']


def get_closest_cluster(x_arange, y_arange, coord):
	x = take_closest(x_arange, coord[0])
	y = take_closest(y_arange, coord[1])
	return [x, y]

def take_closest(arange, num):
    pos = bisect_left(arange, num)
    if pos == 0:
        return arange[0]
    if pos == len(arange):
        return arange[-1]
    before = arange[pos - 1]
    after = arange[pos]
    if after - num < num - before:
       return after
    else:
       return before

def create_coord_cluster_mapping(x_arange, y_arange):
	coord_cluster = {}
	cluster_id = 0

	for i in x_arange:
		coord_cluster[i] = {}
		for j in y_arange:
			coord_cluster[i][j] = cluster_id
			cluster_id += 1

	return coord_cluster


def create_x(doc):
	return [doc["x"], doc["y"], doc["time"]["year"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]


def check_top_n_prob(probabilities, LR_labels, y_train, top=3):
	score = 0
	for index, probability in enumerate(probabilities):
		sorted_prob = sorted(range(len(probability)), reverse=True, key=lambda k: probability[k])[:top]
		sorted_labels = [LR_labels[i] for i in sorted_prob]
		if y_train[index] in sorted_labels:
			score += 1
	accuracy = float(score)/len(probabilities)
	print "check_top_n_prob score ", score, "/", len(probabilities)
	return accuracy


def create_x_y_bulk(docs):
	x_train = []
	y_train = []
	for doc in docs:
		x_train.append(create_x(doc))
		y_train.append(doc["place_id"])
	x_train = numpy.array(x_train)
	y_train = numpy.array(y_train)
	return x_train, y_train


def train_clusters(coord_cluster):
	x_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
	y_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)

	for i in range(len(x_limit_arange) - 1):
		x_gt = x_limit_arange[i]
		x_lt = x_limit_arange[i + 1]

		for j in range(len(y_limit_arange) - 1):
			y_gt = y_limit_arange[j]
			y_lt = y_limit_arange[j + 1]

			cluster_id = coord_cluster[i][j]

			query = {
			    "query": {
			        "bool": {
			            "must": [
			               {
			                   "range": {
			                      "x": {
			                         "from": x_gt,
			                         "to": x_lt
			                      }
			                   }
			               },
			               {
			                   "range": {
			                      "y": {
			                         "from": y_gt,
			                         "to": y_lt
			                      }
			                   }
			               }
			            ]
			        }
			    }
			}

			print "querying..."

			cluster_coords = es.search(index="coords", doc_type="fbtrain", body=query, request_timeout=30000)
			print "fetched"
			x_train, y_train = create_x_y_bulk(cluster_coords["hits"]["hits"])

			print x_train.shape
			print y_train.shape

			model =  LogisticRegression()
			# model = OneVsOneClassifier(SVC(kernel='linear'))

			print "started fitting"

			model.fit(x_train, y_train)

			# print "len", x_train, "\n\n"

			# print model.predict([x_train[0]])

			# print "labels", len(model.classes_)
			LR_labels = model.classes_

			print "score", model.score(x_train, y_train)

			probabilities = model.predict_proba(x_train)

			print "accuracy", check_top_n_prob(probabilities, LR_labels, y_train)

			joblib.dump(model, 'model/model_' + str(cluster_id) + '.pkl', compress=1)
			print "saved"

			exit(0)


if __name__ == "__main__":

	n_cluster = 10000
	factor = 10/math.sqrt(n_cluster)
	factor_arange = numpy.arange(0, 10, factor)
	x_arange = [i+(float(factor)/2) for i in factor_arange]
	y_arange = [i+(float(factor)/2) for i in factor_arange]

	coord_cluster = create_coord_cluster_mapping(x_arange, y_arange)

	train_clusters(coord_cluster)
