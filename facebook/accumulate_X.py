__author__ = 'ayush'

import numpy
import glob
import os


X_coords = numpy.load('X_coords.npy')
X_row_id = numpy.load('X_row_id.npy')

print X_coords.shape
print X_row_id.shape

exit(0)


path = os.path.dirname(os.path.abspath(__file__))

all_X_coords = glob.glob(path + "/X_coords*.npy")
all_X_row_ids = glob.glob(path + "/X_row_id*.npy")

X_coords = []
x = numpy.load(all_X_coords.pop(0))
X_coords = x
for x_file in all_X_coords:
	x = numpy.load(x_file)
	X_coords = numpy.vstack((X_coords, x))
print "X_coords", X_coords.shape
numpy.save("X_coords.npy", X_coords)

X_row_id = []
x = numpy.load(all_X_row_ids.pop(0))
X_row_id = x
for x_file in all_X_row_ids:
	x = numpy.load(x_file)
	X_row_id = numpy.append(X_row_id, x)
print "X_row_id", X_row_id.shape
numpy.save("X_row_id.npy", X_row_id)