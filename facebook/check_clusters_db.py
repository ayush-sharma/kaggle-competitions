__author__ = 'ayush'

from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
fbtrain_db = client['meteor']['fbtrain']
clusters_db = client['meteor']['clusters']

cluster = clusters_db.find_one({"x": 5, "y": 5})

row_ids = cluster["row_ids"]
row_id = row_ids[0]
place_id = int(fbtrain_db.find_one({"row_id": row_id})["place_id"])
print "place_id", row_id, place_id

count = 0
for id in row_ids:
	compare_id = fbtrain_db.find_one({"row_id": id})["place_id"]
	if int(compare_id) == place_id:
		count += 1

print "count", count

print "place_id counts", fbtrain_db.find({"place_id": place_id}).count()