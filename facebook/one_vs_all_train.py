__author__ = 'ayush'

from pymongo import MongoClient
import numpy
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
# from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import log_loss
from bisect import bisect_left
import time


x_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
y_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)

def take_closest(arange, num):
    pos = bisect_left(arange, num)
    if pos == 0:
        return arange[0]
    if pos == len(arange):
        return arange[-1]
    before = arange[pos - 1]
    after = arange[pos]
    if after - num < num - before:
       return after
    else:
       return before

def get_closest_cluster(x_arange, y_arange, coord):
	x = take_closest(x_arange, coord[0])
	y = take_closest(y_arange, coord[1])
	return x, y


def features_product(x):
	x_feat = None
	for i in xrange(0, x.shape[1] - 1):
		for j in xrange(i+1, x.shape[1]):
			col = (x[:, i]*x[:, j]).reshape(x.shape[0], 1)
			if x_feat == None:
				x_feat = col
			else:
				x_feat = numpy.hstack((x_feat, col))
	return x_feat

def get_features(x):
	x_feat_sq = pow(x, 2)
	x_feat_cube = pow(x, 3)
	x_feat = numpy.hstack((x, x_feat_sq, x_feat_cube))
	x_feat = features_product(x_feat)
	return x_feat


def create_x(doc):
	return [doc["x"], doc["y"], doc["time"]["year"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"], doc["time"]["min"]]
	# return [doc["x"], doc["y"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]


def check_top_n_prob(probabilities, LR_labels, y_train, top=3):
	score = 0
	for index, probability in enumerate(probabilities):
		sorted_prob = sorted(range(len(probability)), reverse=True, key=lambda k: probability[k])[:top]
		sorted_labels = [LR_labels[i] for i in sorted_prob]
		if y_train[index] in sorted_labels:
			score += 1
	accuracy = float(score)/len(probabilities)
	print "accuracy score ", score, "/", len(probabilities), accuracy

if __name__ == "__main__":

	client = MongoClient('mongodb://127.0.0.1:3001/meteor')
	fbtrain_db = client['meteor']['fbtrain']
	clusters_db = client['meteor']['clusters']

	all_clusters = clusters_db.find()

	avg_score = 0
	top_features = [228, 229, 237, 245, 121, 138, 83, 112, 119, 120, 153, 207, 217, 221, 222, 227, 267, 270, 21, 43, 84, 102, 103, 130, 208, 218, 268, 42, 55, 137, 145, 146, 182, 210, 258]

	for index, cluster in enumerate(all_clusters):
		print "index", index, cluster["x"], cluster["y"]
		
		row_ids = cluster["row_ids"]

		row_id = row_ids.pop(0)
		doc = fbtrain_db.find_one({"row_id": row_id})
		x = create_x(doc)
		
		x_train = numpy.array([x])
		y_train = numpy.array([doc["place_id"]])

		print "row_ids", len(row_ids)
		for row_id in row_ids:
			doc = fbtrain_db.find_one({"row_id": row_id})
			x = create_x(doc)

			x_train = numpy.vstack((x_train, numpy.array(x)))
			y_train = numpy.append(y_train, doc["place_id"])

		x_all = get_features(x_train)

		x_train = x_all[:, top_features]

		doc_x, doc_y = get_closest_cluster(x_limit_arange, y_limit_arange, [cluster["x"], cluster["y"]])

		try:
			model = joblib.load('LR_models/LR_model_' + str(doc_x) + '_' + str(doc_y) + '.pkl') 
			print "model found"
		except Exception as e:
			print "model not found"
			model = LogisticRegression()
			model.fit(x_train, y_train)

			joblib.dump(model, 'LR_models/LR_model_' + str(cluster["x"]) + "_" + str(cluster["y"]) + '.pkl', compress=1)
			print "saved"

		# start_time = time.time()

		# score = model.score(x_train, y_train)
		# print "score", score
		# avg_score += score

		# LR_labels = model.classes_
		# probabilities = model.predict_proba(x_train)
		# check_top_n_prob(probabilities, LR_labels, y_train)

		# print "TIME: ", time.time() - start_time

		

		# exit(0)

		# if index >= 4:
		# 	print "\navg_score", float(avg_score)/(index+1)
		# 	exit(0)

	# print "\navg_score", float(avg_score)/10201
