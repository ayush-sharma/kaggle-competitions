__author__ = 'ayush'

from pymongo import MongoClient
import numpy
from sklearn.linear_model import LogisticRegression
# from sklearn.multiclass import OneVsOneClassifier
# from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import log_loss


def features_product(x):
	x_feat = None
	for i in xrange(0, x.shape[1] - 1):
		for j in xrange(i+1, x.shape[1]):
			col = (x[:, i]*x[:, j]).reshape(x.shape[0], 1)
			if x_feat == None:
				x_feat = col
			else:
				x_feat = numpy.hstack((x_feat, col))
	return x_feat

def get_features(x):
	x_feat_sq = pow(x, 2)
	x_feat_cube = pow(x, 3)
	x_feat = numpy.hstack((x, x_feat_sq, x_feat_cube))
	x_feat = features_product(x_feat)
	return x_feat


def create_x(doc):
	return [doc["x"], doc["y"], doc["time"]["year"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"], doc["time"]["min"]]
	# return [doc["x"], doc["y"], doc["time"]["month"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["wday"], doc["time"]["hour"]]
	# return [doc["x"], doc["y"], doc["time"]["day"], doc["time"]["wday"], doc["time"]["hour"]]


def check_top_n_prob(probabilities, LR_labels, y_train, top=3):
	score = 0
	for index, probability in enumerate(probabilities):
		sorted_prob = sorted(range(len(probability)), reverse=True, key=lambda k: probability[k])[:top]
		sorted_labels = [LR_labels[i] for i in sorted_prob]
		if y_train[index] in sorted_labels:
			score += 1
	accuracy = float(score)/len(probabilities)
	print "check_top_n_prob score ", score, "/", len(probabilities)
	return accuracy

if __name__ == "__main__":

	client = MongoClient('mongodb://127.0.0.1:3001/meteor')
	fbtrain_db = client['meteor']['fbtrain']
	clusters_db = client['meteor']['clusters']

	all_clusters = clusters_db.find()

	avg_score = 0
	cv = []

	for index, cluster in enumerate(all_clusters):
		print "index", index
		
		row_ids = cluster["row_ids"]

		row_id = row_ids.pop(0)
		doc = fbtrain_db.find_one({"row_id": row_id})
		x = create_x(doc)
		
		x_train = numpy.array([x])
		y_train = numpy.array([doc["place_id"]])

		for row_id in row_ids:
			doc = fbtrain_db.find_one({"row_id": row_id})
			x = create_x(doc)

			x_train = numpy.vstack((x_train, numpy.array(x)))
			y_train = numpy.append(y_train, doc["place_id"])

		x_all = get_features(x_train)
		y_all = y_train

		fold = x_all.shape[0]*0.7

		x_train = x_all[:fold, :]
		y_train = y_all[:fold]

		x_test = x_all[fold:, :]
		y_test = y_all[fold:]


		cf_train = []
		for i in xrange(0, x_train.shape[1]):
			X = x_train[:, i].reshape(x_train.shape[0], 1)
			model = LogisticRegression()
			model.fit(X, y_train)
			pred_y_train = model.predict_proba(X)
			cf = log_loss(y_train, pred_y_train)
			cf_train.append(cf)

		cf_test = []
		for i in xrange(0, x_test.shape[1]):
			X = x_test[:, i].reshape(x_test.shape[0], 1)
			model = LogisticRegression()
			model.fit(X, y_test)
			pred_y_test = model.predict_proba(X)
			cf = log_loss(y_test, pred_y_test)
			cf_test.append(cf)

		cv.append({"train": cf_train, "test": cf_test})

		# joblib.dump(model, 'LR_models/LR_model_' + str(cluster["x"]) + "_" + str(cluster["y"]) + '.pkl', compress=1)
		# print "saved"

		if index >= 9:
			# print "\navg_score", float(avg_score)/(index+1)
			print "cv\n", cv
			exit(0)