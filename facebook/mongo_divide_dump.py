__author__ = 'ayush'

import csv
import numpy
import time
import datetime
from bisect import bisect_left
from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
db = client['meteor']['clusters']
# db.remove({})

db.create_index([
    ("x", 1),
    ("y", 1)
])


def take_closest(arange, num):
    pos = bisect_left(arange, num)
    if pos == 0:
        return arange[0]
    if pos == len(arange):
        return arange[-1]
    before = arange[pos - 1]
    after = arange[pos]
    if after - num < num - before:
       return after
    else:
       return before

def get_closest_cluster(x_arange, y_arange, coord):
	x = take_closest(x_arange, coord[0])
	y = take_closest(y_arange, coord[1])
	return x, y

if __name__ == "__main__":

	bulk = db.initialize_ordered_bulk_op()

	x_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)
	y_limit_arange = numpy.append(numpy.arange(0, 10, 0.1), 10)

	with open('train.csv', 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		headers = next(csvreader, None)
		for count, line in enumerate(csvreader):
			if count % 10000 == 0:
				print "count", count
				try:
					bulk.execute()
					bulk = db.initialize_ordered_bulk_op()
				except Exception as e:
					print "bulk error", e
			row = {}
			row["row_id"] = int(line[0])
			row["x"] = float(line[1])
			row["y"] = float(line[2])

			x, y = get_closest_cluster(x_limit_arange, y_limit_arange, [row["x"], row["y"]])
			x = round(x, 2)
			y = round(y, 2)
			# print "x", x, "y", y
			# db.update({"x": x, "y": y}, {"$push": {"row_ids": row["row_id"]}}, upsert=True)
			bulk.find({"x": x, "y": y}).upsert().update({"$push": {"row_ids": row["row_id"]}})

	try:
		bulk.execute()
	except Exception as e:
		print "bulk error", e

	print "done"


