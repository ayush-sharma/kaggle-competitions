# Kaggle Competitions #

## [Facebook : Predicting Check-Ins](https://www.kaggle.com/c/facebook-v-predicting-check-ins) ##
 
The goal of this competition was to predict which place a person would like to check in to. For a given set of coordinates, the task was to return a ranked list of the most likely places. 

### Data Description ###
* row_id: id of the check-in event
* x y: coordinates
* accuracy: location accuracy 
* time: timestamp
* place_id: id of the business, this is the target to predict

### Prerequisites ###
```
python, mongodb, elasticsearch, sklearn
```

### The Approach ###
1. Dump all the records in a db indexing the key attributes.
2. Then I tried to cluster all the data based on coordinates using k-means, but the data was so huge that even batch-k-means took ages. Consequently, I then clustered using distance measurement considering the location accuracy playing the important role in prediction. 
3. For recommendations, I chose Logistic Regression, but feature selection was the most important call. I chose coordinates, time - year, month, day, week day, hour and min. But these features are not sufficient for better accuracy. Hence, I chose to implement feature mapping and grouped nearby places as one category. 
4. With around 200 features, I had to select the best ones. So, I trained, cross validated 10 clusters with only one feature and did the same for all the 200 features and saved the cost for each of them. 
5. Then, plotting the features led me to find the features which had the least cost in many of the models. From this i chose final 35 features good for better accuracy. 
6. Now, just had to loop through all the test records, find the cluster to which it belongs (wrt coordinates) and considered a top 3 recommendations. 



## Bike Sharing Demand ##

In this competition, participants are asked to combine historical usage patterns with
weather data in order to forecast bike rental demand in the Capital Bike Share program in
Washington, D.C.

## Digit Recognizer ##

The goal in this competition is to take an image of a handwritten single digit, and determine what that digit is.

## Titanic Survival Prediction ##

The challenge was to complete the analysis of what sorts of people were likely to survive
from Titanic Shipwreck Disaster. We need to apply the tools of machine learning to predict
which passengers survived the tragedy.