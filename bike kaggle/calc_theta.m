function theta=calc_theta(X,y,lambda)

X=[ones(size(X,1),1) X];
theta=zeros(size(X,2),1);

options = optimset('GradObj', 'on', 'MaxIter', 50);

    [theta] = ...
        fmincg (@(t)(CostFunc(t, X, y, lambda)), ...
                theta, options);
            
