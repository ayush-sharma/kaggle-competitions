function [J,grad]=CostFunc(theta,X,y,lambda)

m=length(y);
grad = zeros(size(theta));

h=(theta')*(X');

J=((sum((h-y').^2))/(2*m))+((lambda/(2*m))*sum(theta(2:end).^2));

grad(1)=sum(((h-y').*X(:,1)')/m);

for i=2:1:size(grad)
    grad(i)=(sum(((h-y').*X(:,i)')/m))+((lambda/m)*theta(i));
end





% grad=zeros(size(theta));
% m=length(y);
% 
% z=(X*theta)';
% h=sigmoid(z);
% 
% J=((sum(-(y').*log(h)))-(sum((1-y').*log(1-h))))/m+((lambda/(2*m))*sum(theta(2:end).^2));
% 
% grad(1)=(sum((h-y').*(X(:,1)')))/m;
% 
% for i=2:1:size(grad,1)        
%     grad(i)=((sum((h-y').*(X(:,i)')))/m)+((lambda/m)*theta(i));
% end