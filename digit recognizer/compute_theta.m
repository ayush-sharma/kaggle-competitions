function [all_theta]=compute_theta(X,y)

n=size(X,2);

all_theta=zeros(10,n);
lambda=0;

for c=0:1:9
    fprintf('for %d',c);
    initial_theta = zeros(n, 1);
    
    options = optimset('GradObj', 'on', 'MaxIter', 50);

    [theta] = ...
        fmincg (@(t)(costfn(t, X, (y == c), lambda)), ...
                initial_theta, options);
   
    all_theta(c+1,:)=theta;
end