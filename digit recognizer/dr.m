clear all;close all;clc;

A=csvread('train.csv',1,0);

X=A(:,2:end);
m=size(X,1);
y=A(:,1);

X=[ones(m,1) X];

[all_theta]=compute_theta(X,y);

out=predict(all_theta,X);

fprintf('\n*** Accuracy= %d***', mean(double(out == y)) * 100);

test=csvread('test.csv',1,0);
n=size(test,1);
test=[ones(n,1) test];

test_out=predict(all_theta,test);