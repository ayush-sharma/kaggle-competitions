function [out]=predict(all_theta,X)

z=X*(all_theta)';

h=sigmoid(z);

[val,out]=max(h,[],2);
out=out-1;